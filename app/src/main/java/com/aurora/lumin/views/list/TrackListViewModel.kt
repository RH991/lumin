package com.aurora.lumin.views.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.aurora.lumin.data.models.Track
import com.aurora.lumin.data.sources.TrackRepository
import com.aurora.lumin.data.sources.UserRepository
import com.aurora.lumin.data.sources.remote.TrackService.Companion.COUNTRY
import com.aurora.lumin.data.sources.remote.TrackService.Companion.COUNTRY_VALUE_AU
import com.aurora.lumin.data.sources.remote.TrackService.Companion.DEFAULT_SEARCH_TERM
import com.aurora.lumin.data.sources.remote.TrackService.Companion.MEDIA
import com.aurora.lumin.data.sources.remote.TrackService.Companion.MEDIA_VALUE_MOVIE
import com.aurora.lumin.data.sources.remote.TrackService.Companion.TERM
import com.aurora.lumin.data.sources.remote.tools.Resource
import com.aurora.lumin.utils.SingleLiveEvent
import com.aurora.lumin.utils.StringUtils.EMPTY
import com.aurora.lumin.views.BaseViewModel

class TrackListViewModel(
    private val trackRepo: TrackRepository,
    private val userRepo: UserRepository
) : BaseViewModel() {

    val lastTimeVisited = userRepo.lastTimeVisited

    private val _lastTrackVisited = SingleLiveEvent<Track>()
    val lastTrackVisited: LiveData<Track>
        get() = _lastTrackVisited

    private val _tracks = MediatorLiveData<List<Track>>()
    val tracks: LiveData<List<Track>>
        get() = _tracks

    private val _mediator = MediatorLiveData<Any>()
    val mediator: LiveData<Any>
        get() = _mediator

    private var resetTrackId = true

    private val query: Map<String, String> by lazy {
        HashMap<String, String>().apply {
            put(TERM, DEFAULT_SEARCH_TERM)
            put(COUNTRY, COUNTRY_VALUE_AU)
            put(MEDIA, MEDIA_VALUE_MOVIE)
        }
    }


    init {
        restoreTrack()
        refresh()
    }

    fun tryResetTrackId() {
        if (resetTrackId) {
            userRepo.saveLastTrackId(EMPTY)
        }
    }

    private fun restoreTrack() {
        resetTrackId = false

        val source = Transformations.switchMap(userRepo.lastTrackId) {
            trackRepo.getTrack(it)
        }

        _mediator.addSource(source) {
            _mediator.removeSource(source)
            it?.run {
                _lastTrackVisited.value = this
            }
            resetTrackId = true
        }
    }

    fun refresh() {
        val ld = trackRepo.search(query)
        _tracks.addSource(ld) {
            it.run {
                if (status != Resource.Status.LOADING) {
                    _tracks.removeSource(ld)
                }

                data?.results.run {
                    _tracks.value = this
                }

                if (error.isNotEmpty()) {
                    _msg.value = error
                }
            }
        }
    }

}