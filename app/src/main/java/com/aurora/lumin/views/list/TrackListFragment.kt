package com.aurora.lumin.views.list


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.aurora.lumin.R
import com.aurora.lumin.data.models.Track
import com.aurora.lumin.databinding.FragmentTrackListBinding
import com.aurora.lumin.extensions.findNavController
import com.aurora.lumin.extensions.getDimensionPixelSize
import com.aurora.lumin.extensions.resolveAndToast
import com.aurora.lumin.utils.GridSpacingItemDecoration
import com.aurora.lumin.views.adapters.GenericAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class TrackListFragment : Fragment() {

    private val binding: FragmentTrackListBinding by lazy {
        FragmentTrackListBinding.inflate(layoutInflater)
    }

    private val viewModel: TrackListViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        context ?: return binding.root

        val trackAdapter = GenericAdapter(R.layout.layout_track_item,
            object : GenericAdapter.OnItemClickListener<Track> {
                override fun onItemClicked(item: Track) {
                    navigateDetails(item)
                }
            })

        binding.apply {
            viewModel = this@TrackListFragment.viewModel
            lifecycleOwner = this@TrackListFragment
            srRefresh.setOnRefreshListener { this@TrackListFragment.viewModel.refresh() }
            rvList.apply {
                adapter = trackAdapter
                layoutManager = GridLayoutManager(
                    context,
                    DEFAULT_COLUMN_COUNT
                )

                if (itemDecorationCount == 0) {
                    addItemDecoration(
                        GridSpacingItemDecoration(
                            DEFAULT_COLUMN_COUNT,
                            context.getDimensionPixelSize(R.attr.gridItemSpacing)
                        )
                    )
                }
            }

            this@TrackListFragment.viewModel.tracks.observe(viewLifecycleOwner, Observer {
                binding.srRefresh.isRefreshing = false
                trackAdapter.submitList(it)
            })

        }

        viewModel.run {
            mediator.observe(viewLifecycleOwner, Observer {
                // do no thing, for observing livedata inside viewmodel
            })

            lastTrackVisited.observe(viewLifecycleOwner, Observer {
                navigateDetails(it)
            })

            msg.observe(viewLifecycleOwner, Observer {
                context?.resolveAndToast(it)
            })

            tryResetTrackId()
        }

        return binding.root
    }

    private fun navigateDetails(track: Track) {
        val action =
            TrackListFragmentDirections.actionTrackListFragmentToTrackDetailsFragment(track)
        findNavController().navigate(action)
    }

    companion object {
        const val DEFAULT_COLUMN_COUNT = 3
    }

}
