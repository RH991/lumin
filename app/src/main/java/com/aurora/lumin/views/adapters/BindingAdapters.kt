package com.aurora.lumin.views.adapters

import android.text.format.DateUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.aurora.lumin.R
import com.aurora.lumin.data.models.User.Companion.NOT_SET
import com.bumptech.glide.Glide


/**
 * View binding adapters
 */

const val ZERO_MINUTES_AGO = "0 minutes ago"

/**
 * Loads {@code imageUrl} via glide into {@link ImageView}
 */
@BindingAdapter("imageUrl")
fun ImageView.loadImage(imageUrl: String) {
    Glide.with(this.context)
        .load(imageUrl)
        .placeholder(R.drawable.bg_default_track_image)
        .into(this)
}

/**
 * Concatenates the {@code currency} and {@code amount}
 * and set as text value
 */
@BindingAdapter(value = ["currency", "amount"], requireAll = true)
fun TextView.setPrice(currency: String, amount: Double) {
    val price = if (amount == 0.0) context.getString(R.string.free) else
        String.format("%s %s", currency, amount)
    text = price
}

/**
 * Formats {@code time} via {@link DateUtils#getRelativeTimeSpanString}
 * and set as text value
 */
@BindingAdapter("lastTimeVisited")
fun TextView.setLastTimeVisited(time: Long) {
    val temp = DateUtils.getRelativeTimeSpanString(
        time,
        System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS
    )

    val timeStr = if (time == NOT_SET || temp == ZERO_MINUTES_AGO) {
        context.getString(R.string.now)
    } else {
        temp
    }

    text = String.format(context.getString(
            R.string.last_time_visited_format,
            timeStr))
}

