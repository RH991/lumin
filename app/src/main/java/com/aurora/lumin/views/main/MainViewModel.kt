package com.aurora.lumin.views.main

import androidx.lifecycle.ViewModel
import com.aurora.lumin.data.sources.UserRepository

class MainViewModel(private val userRepo: UserRepository) : ViewModel() {
    fun resetLastTimeVisited() {
        userRepo.resetLastTimeVisited()
    }
}