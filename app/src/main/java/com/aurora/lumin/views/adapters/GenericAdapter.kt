package com.aurora.lumin.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.aurora.lumin.BR

/**
 * A generic adapter that can be used for all {@link RecyclerView}
 * adapter that also implements {@link DiffUtil#ItemCallback}
 *
 * @param resId the resource id for the layout
 * @param itemClickListener the click listener that will handle when the item is clicked
 */
class GenericAdapter<T : DiffCallback>(
    @LayoutRes private val resId: Int,
    private val itemClickListener: OnItemClickListener<T>)
    : ListAdapter<T, GenericAdapter.ViewHolder<T>>(DiffCallbackImpl()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(DataBindingUtil.inflate(
        LayoutInflater.from(parent.context), resId, parent, false), itemClickListener)

    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder<T>(
        private val binding: ViewDataBinding,
        private val itemClickListener: OnItemClickListener<T>
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: T) {
            binding.apply {
                setVariable(BR.item, item)
                setVariable(BR.itemListener, itemClickListener)
                executePendingBindings()
            }
        }
    }

    interface OnItemClickListener<T> {
        fun onItemClicked(item: T)
    }
}

interface DiffCallback {
    fun getId(): String
    override fun equals(other: Any?) : Boolean
}

private class DiffCallbackImpl<T : DiffCallback> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T) = oldItem.getId() == newItem.getId()

    override fun areContentsTheSame(oldItem: T, newItem: T) = oldItem == newItem
}