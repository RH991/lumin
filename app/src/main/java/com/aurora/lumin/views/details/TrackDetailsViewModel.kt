package com.aurora.lumin.views.details

import androidx.lifecycle.ViewModel
import com.aurora.lumin.data.models.Track
import com.aurora.lumin.data.sources.UserRepository

class TrackDetailsViewModel(
    private val track: Track,
    userRepository: UserRepository) : ViewModel() {

    init {
        userRepository.saveLastTrackId(track.trackId)
    }

    val image
        get() = track.artworkUrlLargest

    val name
        get() = track.trackName

    val artist
        get() = track.artistName

    val rating
        get() = track.contentAdvisoryRating

    val genre
        get() = track.primaryGenreName

    val currency
        get() = track.currency

    val amount
        get() = track.trackPrice

    val description
        get() = track.longDescription
}