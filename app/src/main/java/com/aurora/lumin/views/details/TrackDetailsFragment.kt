package com.aurora.lumin.views.details


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs

import com.aurora.lumin.databinding.FragmentTrackDetailsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

/**
 * A simple [Fragment] subclass.
 */
class TrackDetailsFragment : Fragment() {

    private val binding: FragmentTrackDetailsBinding by lazy {
        FragmentTrackDetailsBinding.inflate(layoutInflater)
    }

    private val args: TrackDetailsFragmentArgs by navArgs()

    private val viewModel: TrackDetailsViewModel by viewModel {
        parametersOf(args.track)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        context ?: return binding.root

        binding.apply {
            viewModel = this@TrackDetailsFragment.viewModel
            lifecycleOwner = this@TrackDetailsFragment
        }

        return binding.root
    }

}
