package com.aurora.lumin

import android.app.Application
import com.aurora.lumin.di.appModule
import com.aurora.lumin.di.dataModule
import com.aurora.lumin.di.networkModule
import com.aurora.lumin.di.viewModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class LuminApplication : Application() {

    override fun onCreate(){
        super.onCreate()
        startKoin {
            // declare used Android context
            androidContext(this@LuminApplication)
            // modules
            modules(listOf(appModule, dataModule, networkModule, viewModule))
        }
    }
}