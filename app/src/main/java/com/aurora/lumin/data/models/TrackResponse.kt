package com.aurora.lumin.data.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Data model that envelopes the track results
 */
data class TrackResponse (
    @SerializedName("resultCount") val resultCount : Int,
    @SerializedName("results") val results : List<Track>
) : Serializable