package com.aurora.lumin.data.models

import com.aurora.lumin.views.adapters.DiffCallback
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Data model for track
 */
data class Track(
    @SerializedName("trackId") val trackId : String,
    @SerializedName("trackName") val trackName : String,
    @SerializedName("artistName") val artistName : String,
    @SerializedName("contentAdvisoryRating") val contentAdvisoryRating : String,
    @SerializedName("artworkUrl100") val artworkUrl100 : String,
    @SerializedName("currency") val currency : String,
    @SerializedName("trackPrice") val trackPrice : Double,
    @SerializedName("primaryGenreName") val primaryGenreName : String,
    @SerializedName("longDescription") val longDescription : String
    ) : Serializable, DiffCallback {
    override fun getId() = trackId

    val artworkUrlLargest
        get() = artworkUrl100.replace(Regex.fromLiteral(ARTWORK_100_RES_DIMENSION), ARTWORK_HIGH_RES_DIMENSION)

    companion object {
        const val ARTWORK_HIGH_RES_DIMENSION = "225x225"
        const val ARTWORK_100_RES_DIMENSION = "100x100"
    }
}