package com.aurora.lumin.data.sources.remote.tools

import androidx.lifecycle.LiveData
import com.aurora.lumin.data.models.Error
import com.aurora.lumin.data.models.Error.Companion.NO_INTERNET
import com.aurora.lumin.extensions.readValue
import com.aurora.lumin.utils.StringUtils.EMPTY
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.net.UnknownHostException
import java.util.concurrent.atomic.AtomicBoolean

/**
 * A {@link Retrofit} adapter that converts the {@link Call} into a
 * {@likn LiveData} of {@link Resource}.
 */
class LiveDataCallAdapter<R>(private val responseType: Type) :
    CallAdapter<R, LiveData<Resource<R>>> {

    override fun responseType() = responseType

    override fun adapt(call: Call<R>): LiveData<Resource<R>> {
        return object : LiveData<Resource<R>>() {
            private var started = AtomicBoolean(false)
            override fun onActive() {
                super.onActive()
                if (started.compareAndSet(false, true)) {
                    call.enqueue(object : Callback<R> {
                        override fun onResponse(call: Call<R>, response: Response<R>) {
                            val result: Resource<R> = if (response.isSuccessful) {
                                Resource.success(response.body())
                            } else {
                                val err: Error? = response.errorBody()?.string()?.readValue()
                                Resource.error(err?.errorMessage ?: EMPTY)
                            }

                            postValue(result)
                        }

                        override fun onFailure(call: Call<R>, throwable: Throwable) {
                            val msg = if (throwable is UnknownHostException) {
                                NO_INTERNET
                            } else {
                                throwable.message ?: EMPTY
                            }
                            postValue(Resource.error(msg))
                        }
                    })
                }
            }
        }
    }
}