package com.aurora.lumin.data.sources.local

import androidx.lifecycle.LiveData
import com.aurora.lumin.data.sources.local.tools.BaseSharedPref
import com.aurora.lumin.extensions.readValue
import com.aurora.lumin.extensions.writeAsString
import com.aurora.lumin.utils.SingleLiveEvent
import java.io.Serializable
import java.lang.reflect.ParameterizedType

/**
 * Base class for creating a {@link BaseSharedPref} that
 * supports {@link LiveData}. One should just observe the
 * {@link LiveData} and any changes on the {@code sharedPref}
 * will propagated to the observer, thereby very useful on
 * a Single Source of Truth structure
 */
abstract class BaseLocal<T : Serializable>(private var sharedPref: BaseSharedPref) {

    private val clazz: Class<T> by lazy { (this.javaClass.genericSuperclass as ParameterizedType)
        .actualTypeArguments[0] as Class<T> }

    private val result: SingleLiveEvent<T> by lazy { SingleLiveEvent<T>() }

    /**
     * Saves the data to {@link BaseSharedPref} and
     * post the value to the backing {@link LiveData} to propagate
     * the change to the observer
     */
    fun save(data: T) {
        sharedPref.set(data.writeAsString())
        fillData()
    }

    /**
     * Returns the backing {@link LiveData}
     */
    fun get(): LiveData<T> {
        fillData()
        return result
    }

    private fun fillData() {
        val value: T? = sharedPref.get().readValue(clazz)
        result.postValue(value)
    }

    /**
     * Clears the data inside {@link BaseSharedPref}
     */
    fun clear() {
        sharedPref.clear()
    }
}