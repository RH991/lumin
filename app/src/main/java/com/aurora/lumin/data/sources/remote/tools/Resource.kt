package com.aurora.lumin.data.sources.remote.tools

import com.aurora.lumin.data.sources.remote.tools.Resource.Status.ERROR
import com.aurora.lumin.data.sources.remote.tools.Resource.Status.LOADING
import com.aurora.lumin.data.sources.remote.tools.Resource.Status.SUCCESS
import com.aurora.lumin.utils.StringUtils.EMPTY

/**
 * A class that will contain the state of the response from API
 */
class Resource<T> private constructor(
    val status: Status,
    val data: T?,
    val error: String = EMPTY
) {

    enum class Status {
        LOADING, SUCCESS, ERROR
    }

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data)
        }

        fun <T> error(error: String, data: T? = null): Resource<T> {
            return Resource(ERROR, data, error)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(LOADING, data)
        }
    }
}