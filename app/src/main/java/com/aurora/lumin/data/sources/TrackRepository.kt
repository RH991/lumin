package com.aurora.lumin.data.sources

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.aurora.lumin.data.models.Track
import com.aurora.lumin.data.models.TrackResponse
import com.aurora.lumin.data.sources.local.TrackLocal
import com.aurora.lumin.data.sources.remote.TrackService
import com.aurora.lumin.data.sources.remote.tools.NetworkRequest
import com.aurora.lumin.utils.AppExecutors

/**
 * Repository for tracks that handles the API call
 * and persisting the data received from the call
 *
 * @param remote the remote repository where the api call originates
 * @param local the local repository where the data will be save
 * @param executors the executor pool to be use when threading
 */
class TrackRepository(private val remote: TrackService,
                      private val local: TrackLocal,
                      private val executors: AppExecutors) {

    /**
     * Returns the tracks received from the API
     */
    fun search(query: Map<String, String>) = object : NetworkRequest<TrackResponse,
            TrackResponse>(executors) {
        override fun saveCallResult(item: TrackResponse) {
            local.save(item)
        }

        override fun loadFromDb() = local.get()

        override fun createCall() = remote.search(query)

    }.asLiveData()

    /**
     * Returns the {@link Track} that it finds
     */
    fun getTrack(id: String) : LiveData<Track> {
        return  Transformations.map(local.get()) {
            it?.results?.find { u -> u.trackId == id }
        }
    }
}