package com.aurora.lumin.data.sources.local

import com.aurora.lumin.data.models.User
import com.aurora.lumin.data.sources.local.tools.BaseSharedPref

/**
 * A local repository for user data
 */
class UserLocal(sharedPref: BaseSharedPref)
    : BaseLocal<User>(sharedPref)