package com.aurora.lumin.data.models

import com.aurora.lumin.utils.StringUtils.EMPTY
import java.io.Serializable

/**
 * Data model for user data
 */
data class User(var lastTimeVisited: Long = NOT_SET, var lastTrackId: String = EMPTY) : Serializable {
    companion object {
        const val NOT_SET = -1L
    }
}