package com.aurora.lumin.data.models

import com.google.gson.annotations.SerializedName

/**
 * Data model for API error
 */
data class Error(@SerializedName("errorMessage") val errorMessage: String) {
    companion object {
        const val NO_INTERNET = "no_internet"
    }
}