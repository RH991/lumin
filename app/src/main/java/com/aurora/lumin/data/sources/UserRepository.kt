package com.aurora.lumin.data.sources

import androidx.lifecycle.Transformations
import com.aurora.lumin.data.models.User
import com.aurora.lumin.data.sources.local.UserLocal
import com.aurora.lumin.utils.StringUtils.EMPTY

/**
 * Repository for user that handles the API call
 * and persisting the data received from the call
 *
 * @param local the local repository where the data will be save
 */
class UserRepository(private val local: UserLocal) {

    val lastTimeVisited = Transformations.map(local.get()) { user ->
        (user ?: User()).lastTimeVisited
    }

    val lastTrackId
        get() = Transformations.map(local.get()) { user ->
            user?.lastTrackId ?: EMPTY
        }

    /**
     * Resets the last time visited to now
     */
    fun resetLastTimeVisited() = saveUser(local.get().value) {
        it.apply {
            lastTimeVisited = System.currentTimeMillis()
        }
    }

    /**
     * Saves the last track id
     */
    fun saveLastTrackId(id: String) = saveUser(local.get().value) {
        it.apply {
            lastTrackId = id
        }
    }

    private fun saveUser(user: User?, call: (u: User) -> User) {
        local.save(call(user ?: User()))
    }
}