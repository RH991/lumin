package com.aurora.lumin.data.sources.remote

import androidx.lifecycle.LiveData
import com.aurora.lumin.data.models.TrackResponse
import com.aurora.lumin.data.sources.remote.tools.Resource
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface TrackService {

    @GET("search")
    fun search(@QueryMap query: Map<String, String>): LiveData<Resource<TrackResponse>>

    companion object {
        /**
         * The URL-encoded text string you want to search for. For example: jack+johnson.
         */
        const val TERM = "term"
        /**
         * Default search term
         */
        const val DEFAULT_SEARCH_TERM = "star"
        /**
         * The two-letter country code for the store you want to search. The search uses
         * the default store front for the specified country. For example: AU. The default is AU.
         */
        const val COUNTRY = "country"
        /**
         * Australia country
         */
        const val COUNTRY_VALUE_AU = "au"
        /**
         * The media type you want to search for. For example: movie. The default is all.
         */
        const val MEDIA = "media"
        /**
         * Movie media type
         */
        const val MEDIA_VALUE_MOVIE = "movie"
    }
}