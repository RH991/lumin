package com.aurora.lumin.data.sources.local.tools

import android.content.SharedPreferences
import com.aurora.lumin.utils.StringUtils.EMPTY

/**
 * A wrapper class for {@link SharedPreferences}.
 */
class BaseSharedPref(private val preferences: SharedPreferences,
                     private val key: String = EMPTY,
                     private val defaultValue: String = EMPTY) {
    /**
     * Returns {@code true} if set otherwise {@code false}
     */
    val isSet
        get() = preferences.contains(key)

    /**
     * Returns value from the {@link SharedPreferences}
     */
    fun get() = preferences.getString(key, defaultValue) ?: EMPTY

    /**
     * Save the value to {@link SharedPreferences}
     */
    fun set(value: String) {
        preferences.edit().putString(key, value).apply()
    }

    /**
     * Clears the data
     */
    fun clear() {
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
    }
}