package com.aurora.lumin.data.sources.local

import com.aurora.lumin.data.models.TrackResponse
import com.aurora.lumin.data.sources.local.tools.BaseSharedPref

/**
 * A local repository for searched tracks
 */
class TrackLocal(sharedPref: BaseSharedPref)
    : BaseLocal<TrackResponse>(sharedPref)