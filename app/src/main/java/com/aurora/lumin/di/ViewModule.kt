package com.aurora.lumin.di

import com.aurora.lumin.data.models.Track
import com.aurora.lumin.views.details.TrackDetailsViewModel
import com.aurora.lumin.views.list.TrackListViewModel
import com.aurora.lumin.views.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModule = module {
    viewModel { MainViewModel(get()) }
    viewModel { TrackListViewModel(get(), get()) }
    viewModel { (track: Track) -> TrackDetailsViewModel(track, get()) }
}