package com.aurora.lumin.di

import com.aurora.lumin.BuildConfig
import com.aurora.lumin.data.sources.remote.TrackService
import com.aurora.lumin.data.sources.remote.tools.LiveDataCallAdapterFactory
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single {
        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
        retrofit
    }

    single {
        get<Retrofit>().create(TrackService::class.java)
    }
}