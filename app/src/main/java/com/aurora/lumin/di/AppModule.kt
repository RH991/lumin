package com.aurora.lumin.di

import com.aurora.lumin.utils.AppExecutors
import org.koin.dsl.module

val appModule = module {
    single { AppExecutors() }
}