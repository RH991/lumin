package com.aurora.lumin.di

import androidx.preference.PreferenceManager
import com.aurora.lumin.data.sources.TrackRepository
import com.aurora.lumin.data.sources.UserRepository
import com.aurora.lumin.data.sources.local.TrackLocal
import com.aurora.lumin.data.sources.local.UserLocal
import com.aurora.lumin.data.sources.local.tools.BaseSharedPref
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val TRACK_SEARCH_PREF = "track_search_pref"
const val USER_PREF = "user_pref"

val dataModule = module {
    single { PreferenceManager.getDefaultSharedPreferences(androidContext()) }

    /**
     * Track preferences
     */

    single(named(TRACK_SEARCH_PREF)) { BaseSharedPref(get(), TRACK_SEARCH_PREF) }

    single { TrackLocal(get(named(TRACK_SEARCH_PREF))) }

    single { TrackRepository(get(), get(), get()) }

    /**
     * User preferences
     */

    single(named(USER_PREF)) { BaseSharedPref(get(), USER_PREF) }

    single { UserLocal(get(named(USER_PREF))) }

    single { UserRepository(get()) }
}
