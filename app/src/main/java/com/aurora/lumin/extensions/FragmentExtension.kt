package com.aurora.lumin.extensions

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment

/**
 *  Find a {@link NavController} given a local {@link Fragment}.
 *  @return the {@link NavController}
 */
fun Fragment.findNavController() = NavHostFragment.findNavController(this)