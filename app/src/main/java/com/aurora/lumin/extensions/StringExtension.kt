package com.aurora.lumin.extensions

import com.google.gson.Gson

/**
 *  Deserialize the specified Json into an object of the reified type.
 *  @return the converted object
 */
inline fun <reified T> String.readValue() : T? =
    Gson().fromJson<T>(this, T::class.java)

/**
 *  Deserialize the specified Json into an object of the specified {@code clazz}.
 *  @param clazz the class type
 *  @return the converted object
 */
fun <T> String.readValue(clazz: Class<T>) : T? =
    Gson().fromJson<T>(this, clazz)