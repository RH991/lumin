package com.aurora.lumin.extensions

import com.aurora.lumin.utils.StringUtils.EMPTY
import com.google.gson.Gson
import java.io.Serializable

/**
 *  Convert the object to json string
 *  @return the converted string
 */
fun Serializable.writeAsString() = Gson().toJson(this) ?: EMPTY