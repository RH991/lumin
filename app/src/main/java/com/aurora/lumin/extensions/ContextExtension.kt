package com.aurora.lumin.extensions

import android.content.Context
import android.content.res.Resources
import android.widget.Toast
import androidx.annotation.AttrRes
import com.aurora.lumin.utils.StringUtils.EMPTY

/**
 *  Resolves attribute to it's dimension value in pixel
 *  @return the dimension in pixel
 */
fun Context.getDimensionPixelSize(@AttrRes res: Int) : Int {
    val attributes = obtainStyledAttributes(intArrayOf(res))
    val dimension = attributes.getDimensionPixelSize(0, 0)
    attributes.recycle()
    return dimension
}

/**
 * Resolve the {@code value} as a resource then show the {@code msg} as toast message
 */
fun Context.resolveAndToast(value: String) {
    val msg = resolveStringResource(value).let {
        if (it.isEmpty()) {
            value
        } else {
            it
        }
    }

    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}

/**
 * Resolve the {@code value} as a resource
 */
fun Context.resolveStringResource(value: String) : String {
    var ret = EMPTY
    try {
        ret = getString(
            resources.getIdentifier(value,
                "string", packageName))
    } catch (ex: Resources.NotFoundException) {
        // do nothing
    }
    return ret
}