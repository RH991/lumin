Master Detail Application with MVVM + Android Architecture Components + Koin
============================================================================

This is an application that fetches a list of tracks from Itunes Search API.

Introduction
-------------

### Functionality
The app is composed of 2 main screens.
#### TrackListFragment
Displays the list of tracks fetched from Itunes Search API.

![Scheme](images/1.jpg)

#### TrackDetailsFragment
Displays the details of the track which include it's name,
content rating, genre, artist, price and a long description

![Scheme](images/2.jpg)

### MVVM + Data binding
MVVM with Data Binding on Android has the benefits of testability and modularity
while removing the boilerplate code to connect the model to the view.

### Single source of truth (SSOT)
The app implements a SSOT principle wherein the data being pulled will always 
come from the local repository to avoid data inconsistencies when the data could be coming
from multiple data source type i.e, api, disk cache etc.

### Libraries
* [AndroidX Library][androidx]
* [Android Architecture Components][arch]
* [Android Data Binding][data-binding]
* [Navigation][navigation] for navigating screens
* [Koin][koin] for dependency injection
* [Retrofit][retrofit] for REST API communication
* [Glide][glide] for image loading


[androidx]: https://developer.android.com/jetpack/androidx
[arch]: https://developer.android.com/arch
[data-binding]: https://developer.android.com/topic/libraries/data-binding/index.html
[navigation]: https://developer.android.com/guide/navigation
[koin]: https://insert-koin.io/
[retrofit]: http://square.github.io/retrofit
[glide]: https://github.com/bumptech/glide
